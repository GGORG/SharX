#!/usr/bin/env python3
import os
from re import sub
import secrets
import signal
import subprocess
from pathlib import Path

import argh
import sqlalchemy
from tabulate import tabulate

from main import app, on_start
from database import db, User, Invite, InvisibleUrl, Image, Embed

@argh.arg("--host", "-h", help="Specify the IP/host to listen on")
@argh.arg("--port", "-p", help="Specify the port to listen on")
@argh.arg("--debug", "-d", help="This enables the Flask debug mode")
def server(host="0.0.0.0", port=5555, debug=False):
    """Starts a Flask development testing server locally"""
    print("Starting development testing server...")
    app.run(host=host, port=port, debug=debug)


@argh.arg("--pid", "-p", help="Specify a PID manually, otherwise it will be pulled from pid.txt")
def restart_wsgi(pid=0):
    """Restarts the WSGI server"""

    srv =  app.config['SHARX_CONFIG']['wsgi']
    if srv == "None":
        return "Unknown WSGI server, please use either mod_wsgi or Phusion Passenger"
    
    
    if srv == 'mod_wsgi':
        if pid != 0:
            os.kill(pid, signal.SIGINT)
            return
        with open("pid.txt", "r") as f:
            pids = f.readlines()
        for process in pids:
            process = process.strip()
            if process == "":
                continue
            try:
                os.kill(int(process), signal.SIGINT)
                print("Successfully killed process " + process)
            except ProcessLookupError:
                print("Failed to kill process " + process)
        with open("pid.txt", "w") as f:
            f.write("")
    elif srv == 'passenger':
        try:
            proc = subprocess.Popen(["passenger-config", "restart-app", os.getcwd()])
            ret = proc.wait()
            if ret != 0:
                raise Exception()
            else:
                return "Restarted successfully using passenger-config"
        except Exception:
            Path(os.path.join("tmp", "restart.txt")).touch()
            return "Restarted successfully using restart.txt"


def update():
    """Updates the Git repo and restarts the WSGI server"""
    proc = subprocess.Popen(["git", "pull", "--rebase", "--autostash"])
    ret = proc.wait()
    if ret != 0:
        raise Exception("Updating Git repo failed")
    
    proc = subprocess.Popen(["pip", "install", "-r", "requirements.txt"])
    ret = proc.wait()
    if ret != 0:
        raise Exception("Updating pip packages failed")
    return restart_wsgi()


def users_list():
    with app.app_context():
        users = User.query.all()
        new_users = [
            [
                user.uid,
                user.username,
                user.email,
                user.password_hash,
                user.key,
                user.storage_used,
                user.invisibleurls,
            ]
            for user in users
        ]

        return tabulate([['uid', 'username', 'email', 'password_hash', 'key', 'storage_used', 'invisibleurls']] + new_users,
                        headers='firstrow', tablefmt='fancy_grid')


@argh.arg("--uid", "-i")
@argh.arg("--username", "-n")
def users_del(uid=-1, username=""):
    with app.app_context():
        if uid != -1:
            user: User = User.query.filter_by(uid=uid).first()
            username = user.username
        elif username != "":
            user: User = User.query.filter_by(username=username).first()
            uid = user.uid
        else:
            return "Please provide either a username or a UID!"

    print(f"Deleting user {username}: ")
    print(tabulate([['uid', 'username', 'email', 'password_hash', 'key', 'storage_used', 'invisibleurls'],
                    [
                        user.uid,
                        user.username,
                        user.email,
                        user.password_hash,
                        user.key,
                        user.storage_used,
                        user.invisibleurls,
                    ]], headers='firstrow', tablefmt='fancy_grid'))
    proceed = input("Do you want to proceed? [Y/n]: ")
    if proceed.lower() not in ["", "y"]:
        return "Cancelled"

    with app.app_context():
        usr_images = Image.query.fiter_by(user=uid).all()
    for img in usr_images:
        file = img.id + img.ext
        os.remove(os.path.join(config['storage_folder'], file))

    with app.app_context():
        db.session.delete(user)
        db.session.commit()

    return "User deleted successfully!"


def users_set(uid: int, key: str, value: str):
    with app.app_context():
        user: User = User.query.filter_by(uid=uid).first()

    print("Before:")
    print(tabulate([['uid', 'username', 'email', 'password_hash', 'key', 'storage_used', 'invisibleurls'],
                    [
                        user.uid,
                        user.username,
                        user.email,
                        user.password_hash,
                        user.key,
                        user.storage_used,
                        user.invisibleurls,
                    ]], headers='firstrow', tablefmt='fancy_grid'))
    print("After:")
    if isinstance(user.__getattribute__(key), int):
        value: int = int(value)
    user.__setattr__(key, value)
    print(tabulate([['uid', 'username', 'email', 'password_hash', 'key', 'storage_used', 'invisibleurls'],
                    [
                        user.uid,
                        user.username,
                        user.email,
                        user.password_hash,
                        user.key,
                        user.storage_used,
                        user.invisibleurls,
                    ]], headers='firstrow', tablefmt='fancy_grid'))
    proceed = input("Do you want to proceed? [Y/n]: ")
    if proceed.lower() in ["", "y"]:
        with app.app_context():
            db.session.commit()


        return "Property changed successfully"
    else:
        return "Cancelled"


def users_getuid(username: str):
    with app.app_context():
        user: User = User.query.filter_by(username=username).first()
    uid = user['uid']

    return f"User with username {username} has UID {uid}"


@argh.arg("--uid", "-i")
@argh.arg("--username", "-n")
def users_get(uid=-1, username=""):
    with app.app_context():
        if uid != -1:
            user: User = User.query.filter_by(uid=uid).first()
        elif username != "":
            user: User = User.query.filter_by(username=username).first()
        else:
            return "Please provide either a username or a UID!"

        return tabulate([['uid', 'username', 'email', 'password_hash', 'key', 'storage_used', 'invisibleurls'],
                        [
                            user.uid,
                            user.username,
                            user.email,
                            user.password_hash,
                            user.key,
                            user.storage_used,
                            user.invisibleurls,
                        ]], headers='firstrow', tablefmt='fancy_grid')


def stats():
    with app.app_context():
        img_count = len(Image.query.all())
        users = User.query.all()
        usr_count = len(users)
        total_storage = round(sum(user.storage_used for user in users) / (1024 * 1024), 2)
    return tabulate(
        {"Image count": [img_count], "User count": [usr_count], "Storage used": [str(total_storage) + " MB"]},
        headers='keys', tablefmt='fancy_grid')


def imgs_del(id: str):
    with app.app_context():
        img: Image = Image.query.filter_by(id=id).first()

    print(f"Deleting image {id}: ")
    print(tabulate([['id', 'name', 'ext', 'upload_time', 'size_b', 'user'],
                    [
                            img.id,
                            img.name,
                            img.ext,
                            img.upload_time,
                            img.size_b,
                            img.user
                    ]], headers='firstrow', tablefmt='fancy_grid'))
    proceed = input("Do you want to proceed? [Y/n]: ")
    if proceed.lower() in ["", "y"]:
        with app.app_context():
            user: User = User.query.filter_by(uid=img.user).first()
            os.remove(os.path.join(config['storage_folder'], str(
                user.uid), id + img.ext))
            user.storage_used -= img.size_b
            db.session.delete(img)

        return "Image deleted successfully!"
    else:
        return "Cancelled"


@argh.arg("--uid", "-i")
@argh.arg("--username", "-n")
def users_img_count(uid=-1, username=""):
    with app.app_context():
        if uid != -1:
            user: User = User.query.filter_by(uid=uid).first()
        elif username != "":
            user: User = User.query.filter_by(username=username).first()
        else:
            return "Please provide either a username or a UID!"
        imgs = len(Image.query.filter_by(user=user.uid).all())
        return str(imgs)


@argh.arg("--count", "-c", help="Specifies the amount of best users to return")
def users_best(count=5):
    with app.app_context():
        users = User.query.all()
        users_with_img_count = []
        for user in users:
            imgs = len(Image.query.filter_by(user=user.uid).all())
            users_with_img_count.append(
                (user.uid, user.username, round(user.storage_used / (1024 * 1024), 2), imgs))

        users_with_img_count = list(sorted(users_with_img_count, key=lambda user: user[3]))
        users_top = users_with_img_count[-count:]
        users_top.reverse()
        return tabulate(
            [['uid', 'username', 'storage_used_mb', 'image_count']] + users_top,
            headers='firstrow', tablefmt='fancy_grid')


@argh.arg("--count", "-c", help="Specifies the amount of invites")
def invites_wave(count=1):
    with app.app_context():
        users = User.query.all()
        for user in users:
            for _ in range(count):
                code = secrets.token_hex(8)
                inv: Invite = Invite(code=code, user=user.uid)
                db.session.add(inv)
                db.session.commit()
        return f"Invite wave of {count} invite(s) successful!"


@argh.arg("--uid", "-i")
@argh.arg("--username", "-n")
@argh.arg("--count", "-c", help="Specifies the amount of invites")
def invites_gen(uid=-1, username="", count=1):
    with app.app_context():
        if uid != -1:
            user: User = User.query.filter_by(uid=uid).first()
        elif username != "":
            user: User = User.query.filter_by(username=username).first()
        else:
            return "Please provide either a username or a UID!"
        for _ in range(count):
            code = secrets.token_hex(8)
            inv: Invite = Invite(code=code, user=user.uid)
            db.session.add(inv)
            db.session.commit()
        return f"Successfully gave {count} invite(s) to {user.username} (uid {user.uid})"


def invites_revoke(code: str):
    with app.app_context():
        inv: Invite = Invite.query.filter_by(code=code).first()
        user: User = User.query.filter_by(uid=inv.user).first()
        db.session.delete(inv)
        db.session.commit()
        return f"Successfully revoked invite {code} from {user.username} (uid {user.uid})"


@argh.arg("--table", "-t")
def db_query(sql: str, table: bool = False):
    with app.app_context():
        with db.get_engine().connect() as con:
            rows = con.execute(sql)
            try:
                output = [tuple(row) for row in rows]
            except sqlalchemy.exc.ResourceClosedError:
                return "*** Empty response"

            if table:
                return tabulate(output, tablefmt='fancy_grid')
            else:
                return output

if __name__ == "__main__":
    global config
    on_start()
    config: dict = app.config['SHARX_CONFIG']
    parser = argh.ArghParser()
    parser.add_commands([server, restart_wsgi, update, users_list, users_del, users_set, users_getuid, users_get, stats, imgs_del, users_img_count, users_best,
         invites_gen, invites_wave, invites_revoke, db_query])
    parser.dispatch()
