from datetime import datetime
from functools import wraps

from flask import redirect, session, url_for, flash, current_app

from database import *


def login_required(func):
    @wraps(func)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            if session['logged_in']:
                if 'uid' in session:
                    user: User = User.query.filter_by(uid=session['uid']).first()
                    if user is None:
                        del session['uid']
                        return redirect(url_for("users.logout"))

                return func(*args, **kwargs)
        else:
            flash("You need to login first", 'warning')
            return redirect(url_for('users.login'))

    return wrap


def process_embed(embed: Embed, image: Image, user: User):
    space = round(user.storage_used / (1024 * 1024), 2)

    images = Image.query.filter_by(user=user.uid).all()

    replace_dict = {'$user.name$': user.username, '$user.uid$': user.uid, '$user.email$': user.email,
                    '$user.img_count$': len(images), '$user.used_space$': space, '$img.name$': image.name,
                    '$img.id$': image.id, '$img.ext$': image.ext,
                    '$img.uploaded_at.timestamp$': image.upload_time,
                    '$img.uploaded_at.utc$': datetime.utcfromtimestamp(image.upload_time).strftime(
                        "%d.%m.%Y %H:%M"),
                    '$img.size$': str(round(image.size_b / 1024, 2)),
                    '$host.name$': current_app.config['SHARX_CONFIG']['name'],
                    '$host.motd$': current_app.config['SHARX_CONFIG']['motd']}

    for a, b in replace_dict.items():
        embed.title = embed.title.replace(str(a), str(b))
        embed.desc = embed.desc.replace(str(a), str(b))
        embed.author_name = embed.author_name.replace(str(a), str(b))
        embed.author_url = embed.author_url.replace(str(a), str(b))
        embed.provider_name = embed.provider_name.replace(str(a), str(b))
        embed.provider_url = embed.provider_url.replace(str(a), str(b))

    return embed
