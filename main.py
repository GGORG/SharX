import json
import os
import shutil
import subprocess

from flask import Flask, render_template, g
from flask.wrappers import Request

from database import *

app: Flask = Flask(__name__)

pid_write_empty = False


# UTILITY

def writepid():
    global pid_write_empty
    try:
        with open("pid.txt", "r") as f:
            lines = f.readlines()
            if len(lines) >= 2:
                pid_write_empty = True
    except OSError:
        pass
    if pid_write_empty:
        with open("pid.txt", "w") as f:
            f.write("")
    with open("pid.txt", "a") as f:
        f.write(str(os.getpid()) + "\n")


config: dict

# DB STUFF


def get_version():
    try:
        return subprocess.check_output(["git", "describe", "--tags"]).strip().decode()
    except Exception:
        return "?"




def setup():
    global config

    # Config
    if not os.path.exists("config"):
        os.mkdir("config")

    if not os.path.exists(os.path.join("config", "config.json")):
        shutil.copy("config.json.example",
                    os.path.join("config", "config.json"))

    with open(os.path.join("config", "config.json"), "r") as f:
        config = json.load(f)

    with open("config.json.example", "r") as f:
        config_example = json.load(f)

    for key, item in config_example.items():
        if key not in list(config.keys()):
            config[key] = item
            print(f"Adding item {key} to the config")
        if not isinstance(config[key], type(item)):
            config[key] = item
            print(f"Fixing item {key} in the config")

    if config['storage_folder'].endswith("/"):
        config['storage_folder'] = config['storage_folder'][:-1]

    if config['sql_db_uri'] == '[default]':
        config['sql_db_uri'] = 'sqlite:///' + os.path.abspath(os.path.join("config", "data.db"))

    with open(os.path.join("config", "config.json"), "w") as f:
        json.dump(config, f, indent=4)

    app.config['SHARX_HOST_NAME'] = config['name']
    app.config['SHARX_HOST_MOTD'] = config['motd']
    app.config['SHARX_CONFIG'] = config
    with app.app_context():
        g.sharx_config = config
    app.config['SHARX_HOST_VERSION'] = get_version()

    app.config['DEBUG'] = config['flask_debug']
    app.config['SQLALCHEMY_ECHO'] = config['sqla_debug']
    app.config['SQLALCHEMY_DATABASE_URI'] = config['sql_db_uri']

    # Secret key
    if not os.path.exists(os.path.join("config", "secret_key.txt")):
        with open(os.path.join("config", "secret_key.txt"), "wb") as f:
            f.write(os.urandom(16))

    with open(os.path.join("config", "secret_key.txt"), "rb") as f:
        app.config['SECRET_KEY'] = f.read()

    # Storage folder
    if not os.path.exists(config['storage_folder']):
        os.mkdir(config['storage_folder'])


    with app.app_context():
        db.create_all()


# ERROR HANDLERS

@app.errorhandler(404)
def not_found(e):
    return render_template("404.html", error=e), 404


@app.errorhandler(405)
def method_not_allowed():
    return render_template("405.html"), 405


@app.errorhandler(500)
def internal_server_error(e):
    return render_template("500.html", error=e), 500


def register_blueprints():
    # BLUEPRINTS

    from blueprints import api, links, images
    from blueprints.dashboard import configs, dashboard, users

    app.register_blueprint(api.app)
    app.register_blueprint(images.app)
    app.register_blueprint(links.app)
    app.register_blueprint(users.app)
    app.register_blueprint(dashboard.app)
    app.register_blueprint(configs.app)


def register_extensions():
    db.init_app(app)


def on_start():
    register_extensions()
    setup()
    register_blueprints()

def wsgi_start():
    writepid()
    on_start()

    if 'mod_wsgi.process_group' in os.environ and os.environ['mod_wsgi.process_group'] != '':
        config['wsgi'] = 'mod_wsgi'
    elif 'IN_PASSENGER' in os.environ and os.environ['IN_PASSENGER'] == '1':
        config['wsgi'] = 'passenger'
    with open(os.path.join("config", "config.json"), "w") as f:
        json.dump(config, f, indent=4)
    app.config['SHARX_CONFIG'] = config
    with app.app_context():
        g.sharx_config = config

if __name__ == '__main__':
    print("Starting Flask development server")
    on_start()


    config['wsgi'] = 'flask'
    with open(os.path.join("config", "config.json"), "w") as f:
        json.dump(config, f, indent=4)
    app.config['SHARX_CONFIG'] = config
    with app.app_context():
        g.sharx_config = config


    app.run()
