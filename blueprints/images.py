import os
from datetime import datetime
from PIL import Image as PILImage
import random
import secrets
import time
import imagehash


from flask import request, send_from_directory, render_template, redirect, url_for, jsonify, \
    Blueprint, current_app

from database import *
from utils import process_embed


app = Blueprint('images', __name__)


### IMAGES AND STUFF

@app.route("/i/<id>", methods=['GET', 'DELETE'])
def get_img(id):
    if id.endswith('\u200B'):
        invisibleurl: InvisibleUrl = InvisibleUrl.query.filter_by(url=id).first_or_404(
            description='Invalid invisible URL')

        id = invisibleurl.imgid

        return redirect(url_for("images.get_img", id=id))

    if request.method == 'DELETE':
        return redirect(url_for('dashboard.delete_image', id=id))

    image: Image = Image.query.filter_by(id=id).first_or_404(description='Image not found')
    user: User = User.query.filter_by(uid=image.user).first()
    embed: Embed = Embed.query.filter_by(user=image.user).first()

    if embed is None:
        embed = Embed(user=user.uid, color="", title="", desc="", author_name="", author_url="", provider_name="", provider_url="")

    embed = process_embed(embed, image, user)
    embed_adv = embed.author_name != "" or embed.author_url != "" or embed.provider_name != "" or embed.provider_url != ""

    color_on = embed.color != ""
    title_on = embed.title != ""
    desc_on = embed.desc != ""

    return render_template(
        "image.html",
        img_name=image.name,
        img_id=image.id,
        img_ext=image.ext,
        size_kb=str(round(image.size_b / 1024, 2)),
        size_mb=str(round(image.size_b / (1024 * 1024), 2)),
        uploaded_by=user.username,
        uploaded_uid=user.uid,
        uploaded_at=datetime.utcfromtimestamp(
            image.upload_time
        ).strftime("%d.%m.%Y %H:%M"),
        embed_color=embed.color,
        embed_title=embed.title,
        embed_desc=embed.desc,
        embed_adv=embed_adv,
        embed_color_on=color_on,
        embed_title_on=title_on,
        embed_desc_on=desc_on,
    )


@app.route("/<id>")
def root_img(id):
    return redirect(url_for("images.get_img", id=id))


@app.route("/i/raw/<id>")
def img_raw(id):
    img: Image = Image.query.filter_by(id=id).first_or_404(description='Image not found')
    usr = img.user
    dir = os.path.join(current_app.config['SHARX_CONFIG']['storage_folder'], str(usr))
    filename = img.id + img.ext

    return send_from_directory(dir, filename)


@app.route("/i/embed/<id>")
def get_embed(id):
    image: Image = Image.query.filter_by(id=id).first_or_404(description='Image not found')
    user: User = User.query.filter_by(uid=image.user).first()
    embed: Embed = Embed.query.filter_by(user=image.user).first()

    if embed is None:
        embed = Embed(user=user.uid, color="", title="", desc="", author_name="", author_url="", provider_name="", provider_url="")

    embed = process_embed(embed, image, user)

    em_json = {
        'type': 'link',
        'version': '1.0'
    }
    if embed.author_name != "":
        em_json['author_name'] = embed.author_name
    if embed.author_url != "":
        em_json['author_url'] = embed.author_url
    if embed.provider_name != "":
        em_json['provider_name'] = embed.provider_name
    if embed.provider_url != "":
        em_json['provider_url'] = embed.provider_url

    return jsonify(em_json)

@app.route("/upload", methods=['POST', 'GET'])
def upload():
    if request.method == 'GET':
        return render_template("405.html"), 405
    attributes = request.form.to_dict(flat=False)
    if 'uid' not in list(attributes.keys()) or 'key' not in list(attributes.keys()):
        return "Required value not in request attributes", 400
    elif 'image' not in list(request.files.to_dict(flat=False).keys()):
        return "'image' not provided", 400

    user: User = User.query.filter_by(uid=attributes['uid'][0]).first()

    if user is None:
        return "User not found", 401

    if attributes['key'][0] != user.key:
        return "Wrong upload key", 401
    file = request.files['image']
    name, ext = os.path.splitext(file.filename)
    # filename = file.filename
    file.flush()
    size = os.fstat(file.fileno()).st_size
    if ext not in current_app.config['SHARX_CONFIG']['allowed_extensions']:
        return "Unsupported file type", 415
    elif size > 6000000:
        return 'File size too large', 400

    image = PILImage.open(file)
    data = list(image.getdata())
    file_without_exif = PILImage.new(image.mode, image.size)
    file_without_exif.putdata(data)

    avhash = str(imagehash.average_hash(file_without_exif))

    imghash: ImgHash = ImgHash.query.filter_by(hash=avhash, user=int(attributes['uid'][0])).first()
    if imghash is not None:
        ex_image: Image = Image.query.filter_by(id=imghash.imgid).first()
        if user.invisibleurls != 1:
            return jsonify({"url": url_for("images.get_img", id=ex_image.id, _external=True),
                            "raw": url_for("images.img_raw", id=ex_image.id, _external=True)})


        iurl: InvisibleUrl = InvisibleUrl.query.filter_by(imgid=ex_image.id).first()
        if iurl is None:
            ok = False
            while not ok:
                charset = ['\u200B', '\u2060', '\u180E', '\u200D', '\u200C']
                invisible_id = [random.choice(charset) for _ in range(24)]
                invisible_id.append('\u200B')
                invisible_id = ''.join(invisible_id)
                existing: InvisibleUrl = InvisibleUrl.query.filter_by(url=invisible_id).first()
                ok = existing is None

            iurl: InvisibleUrl = InvisibleUrl(url=invisible_id, imgid=ex_image.id)
            db.session.add(iurl)
            db.session.commit()

        return jsonify({"url": url_for("images.root_img", id="", _external=True) + iurl.url,
                        "raw": url_for("images.img_raw", id=ex_image.id, _external=True)})
                        

    img_id = secrets.token_urlsafe(5)
    imghash = ImgHash(hash=avhash, imgid=img_id, user=int(attributes['uid'][0]))
    db.session.add(imghash)
    filename = img_id + ext
    if not os.path.exists(os.path.join(current_app.config['SHARX_CONFIG']['storage_folder'], str(user.uid))):
        os.mkdir(os.path.join(current_app.config['SHARX_CONFIG']['storage_folder'], str(user.uid)))
    file_without_exif.save(os.path.join(
        current_app.config['SHARX_CONFIG']['storage_folder'], str(user.uid), filename))
    user.storage_used += size
    img: Image = Image(id=img_id, name=name, ext=ext, upload_time=round(time.time()), size_b=size,
                       user=attributes['uid'][0])
    db.session.add(img)
    db.session.commit()
    if user.invisibleurls == 1:
        ok = False
        while not ok:
            charset = ['\u200B', '\u2060', '\u180E', '\u200D', '\u200C']
            invisible_id = [random.choice(charset) for _ in range(24)]
            invisible_id.append('\u200B')
            invisible_id = ''.join(invisible_id)
            existing: InvisibleUrl = InvisibleUrl.query.filter_by(url=invisible_id).first()
            ok = existing is None

        url: InvisibleUrl = InvisibleUrl(url=invisible_id, imgid=img_id)
        db.session.add(url)
        db.session.commit()
        return jsonify({"url": url_for("images.root_img", id="", _external=True) + invisible_id,
                        "raw": url_for("images.img_raw", id=img_id, _external=True)})

    return jsonify({"url": url_for("images.get_img", id=img_id, _external=True),
                    "raw": url_for("images.img_raw", id=img_id, _external=True)})

