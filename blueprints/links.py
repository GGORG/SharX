from flask import send_from_directory, render_template, redirect, Blueprint, current_app

from database import *

app = Blueprint('links', __name__)


### LINKS

@app.route("/discord/")
def discord():
    return redirect(f"https://discord.gg/{current_app.config['SHARX_CONFIG']['discord']}", code=301)


@app.route("/favicon.ico")
def favicon():
    return send_from_directory("config", "favicon.ico")


@app.route("/logo.png")
def logo():
    return send_from_directory("config", "logo.png")


@app.route("/")
def home():
    img_count = len(Image.query.all())
    usr_count = len(User.query.all())
    users = User.query.all()
    total_storage = round(sum(user.storage_used
                              for user in users) / (1024 * 1024), 2)
    return render_template("index.html",
                           user_count=usr_count,
                           storage_used=total_storage, image_count=img_count)


@app.route('/repo')
def repo():
    # try:
    #     link = subprocess.check_output(["git", "remote", "get-url", 'origin']).strip().decode()
    #     if link.startswith('git@') or link.startswith('git://') or link.startswith('ssh://'):
    #         link = link.replace('git@', '').replace(':', '/').replace('.git', '')
    # except Exception:
    #     link = "https://gitlab.com/GGORG/SharX"
    link = "https://gitlab.com/GGORG/SharX"

    return redirect(link, code=301)
