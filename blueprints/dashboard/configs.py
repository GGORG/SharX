from flask import session, url_for, jsonify, \
    make_response, Blueprint, current_app

from database import *
from utils import login_required

app = Blueprint('configs', __name__, url_prefix='/dashboard')


# CONFIGS

@app.route("/sharex-config/")
@login_required
def sharex_config():
    user: User = User.query.filter_by(uid=session['uid']).first()
    cfg = {
        "Version": "13.5.0",
        "Name": current_app.config['SHARX_CONFIG']['name'],
        "DestinationType": "ImageUploader",
        "RequestMethod": "POST",
        "RequestURL": url_for("images.upload", _external=True),
        "Body": "MultipartFormData",
        "Arguments": {
            "uid": str(user.uid),
            "key": user.key
        },
        "FileFormName": "image",
        "URL": "$json:url$",
        "ThumbnailURL": "$json:raw$",
        "ErrorMessage": "$response$"
    }
    resp = make_response(jsonify(cfg))

    resp.headers['Content-Disposition'] = 'attachment; filename=config.sxcu'
    resp.content_type = 'application/json; charset=utf-8'

    return resp


@app.route("/sharenix-config/")
@login_required
def sharenix_config():
    user: User = User.query.filter_by(uid=session['uid']).first()
    cfg = {
        "Name": current_app.config['SHARX_CONFIG']['name'],
        "RequestType": "POST",
        "RequestURL": url_for("images.upload", _external=True),
        "Body": "MultipartFormData",
        "Arguments": {
            "uid": str(user.uid),
            "key": user.key
        },
        "FileFormName": "image",
        "URL": "$json:url$",
    }
    resp = make_response(jsonify(cfg))

    resp.headers['Content-Disposition'] = 'attachment; filename=config.json'
    resp.content_type = 'application/json; charset=utf-8'

    return resp
