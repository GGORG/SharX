import json
import os
import random
import string

from flask import request, render_template, redirect, session, url_for, flash, Blueprint, current_app
from passlib.hash import sha256_crypt
from wtforms import Form, StringField, PasswordField, validators, BooleanField

from database import *
from utils import login_required

app = Blueprint('users', __name__, url_prefix='/dashboard')


# USER SYSTEM


class LoginForm(Form):
    username = StringField('Username', [validators.DataRequired()])
    password = PasswordField('Password', [
        validators.DataRequired(),
    ])


@app.route("/login/", methods=['GET', 'POST'])
def login():
    if 'logged_in' in session:
        if session['logged_in']:
            return redirect(url_for("dashboard.dashboard"))
    else:
        form = LoginForm(request.form)
        if request.method == 'POST' and form.validate():
            username = form.username.data
            valid = False
            user: User = User.query.filter_by(username=username).first()
            if user is None:
                valid = False
            elif sha256_crypt.verify(form.password.data, user.password_hash):
                valid = True
            else:
                valid = False

            if valid:
                session['logged_in'] = True
                session['uid'] = user.uid
                flash("Logged in successfully.", 'success')

                return redirect(url_for("dashboard.dashboard"))
            else:
                flash("Invalid username or password", 'danger')

                return render_template("login.html",
                                       form=form)

        return render_template("login.html", form=form)


@app.route("/logout/")
@login_required
def logout():
    session.clear()
    flash("You have been successfully logged out.", 'success')
    return redirect(url_for("links.home"))


class RegistrationForm(Form):
    username = StringField(
        'Username', [validators.Length(min=4, max=20)])
    email = StringField(
        'Email Address', [validators.DataRequired()], render_kw={"type": "email"})
    password = PasswordField('New Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField(
        'Repeat Password', [validators.DataRequired()])
    with open(os.path.join("config", "config.json"), "r") as f:
        config = json.load(f)
        if config['enable_invites']:
            invite = StringField(
                'Invite', [validators.Length(min=16, max=16)])
    agree = BooleanField('I agree to the rules in the Discord server above', [
        validators.DataRequired()])


@app.route("/register/", methods=['GET', 'POST'])
def register():
    if 'logged_in' in session:
        if session['logged_in']:
            return redirect(url_for("dashboard.dashboard"))
    else:
        form = RegistrationForm(request.form)
        if request.method == 'POST' and form.validate():
            username = form.username.data
            email = form.email.data
            password = sha256_crypt.hash((str(form.password.data)))
            user: User = User.query.filter_by(username=username).first()
            if user is not None:
                flash("This username is already taken, please choose another", 'danger')

                return render_template("login.html",
                                       form=form)

            if current_app.config['SHARX_CONFIG']['enable_invites']:
                inv: Invite = Invite.query.filter_by(code=form.invite.data).first()
                if inv is None:
                    flash("Invalid invite code!", 'danger')

                    return render_template("login.html",

                                           form=form)
                else:
                    inviter: User = User.query.filter_by(uid=inv.user).first()
                    if inviter is not None:
                        flash(
                            f"You have been invited by {inviter.username} (uid {inviter.uid})!", 'info')
                    db.session.delete(inv)
                    db.session.commit()

            try:
                latest_uid = User.query.order_by(User.uid).all()[-1].uid
            except IndexError:
                latest_uid = None
            latest_uid = 0 if latest_uid is None else int(latest_uid)

            usr = User(uid=latest_uid + 1, username=username, email=email, password_hash=password,
                       key=f"{username}_{''.join(random.choice(string.ascii_letters) for _ in range(10))}",
                       storage_used=0, invisibleurls=0)
            db.session.add(usr)
            db.session.commit()

            session['logged_in'] = True
            session['uid'] = str(usr.uid)
            flash("Logged in successfully.", 'success')

            return redirect(url_for("dashboard.dashboard"))

        return render_template("register.html",
                               form=form)
