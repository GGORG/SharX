import os
import random
import re
import string

from flask import request, render_template, redirect, session, url_for, flash, Blueprint, current_app
from passlib.hash import sha256_crypt
from wtforms import Form, StringField, PasswordField, validators, TextAreaField, BooleanField

from database import *
from utils import login_required

app = Blueprint('dashboard', __name__, url_prefix='/dashboard')


# DASHBOARD


@app.route("/")
@login_required
def dashboard():
    user: User = User.query.filter_by(uid=session['uid']).first()
    images = Image.query.filter_by(user=user.uid).all()
    invites = Invite.query.filter_by(user=user.uid).all()
    space = round(user.storage_used / (1024 * 1024), 2)
    return render_template(
        "dashboard.html",
        username=user.username,
        img_count=len(images),
        uid=user.uid,
        invites=invites,
        inv_count=len(invites),
        space=space,
    )


@app.route("/embed-conf/", methods=['GET', 'POST'])
@login_required
def embed_conf():
    user: User = User.query.filter_by(uid=session['uid']).first()
    embed: Embed = Embed.query.filter_by(user=user.uid).first()
    add = embed is None
    embed = embed or Embed(user=user.uid, color="", title="", desc="", author_name="", author_url="", provider_name="", provider_url="")

    class EmbedConfigForm(Form):
        color = StringField("Color (hex code)", default=embed.color)
        title = TextAreaField("Title", default=embed.title)
        desc = TextAreaField("Description", default=embed.desc)
        author_name = TextAreaField(
            "Author name", default=embed.author_name)
        author_url = StringField(
            "Author URL", default=embed.author_url, render_kw={"type": "url"})
        provider_name = TextAreaField(
            "Site name", default=embed.provider_name)
        provider_url = StringField(
            "Site URL", default=embed.provider_url, render_kw={"type": "url"})

    form = EmbedConfigForm(request.form)
    if request.method == 'POST' and form.validate():
        if form.color.data != "":
            regex = r"^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$"
            regex = re.compile(regex)
            if regex.fullmatch(form.color.data):
                embed.color = form.color.data
            else:
                flash("Color must be empty or a hex code!", 'warning')
        else:
            embed.color = ""
        embed.title = form.title.data
        embed.desc = form.desc.data
        embed.author_name = form.author_name.data
        embed.author_url = form.author_url.data
        embed.provider_name = form.provider_name.data
        embed.provider_url = form.provider_url.data

        if add:
            db.session.add(embed)

        db.session.commit()

        flash("Embed preferences successfully set!", 'success')

    vars = ['$user.name$', '$user.uid$', '$user.email$', '$user.img_count$', '$user.used_space$', '$img.name$',
            '$img.id$',
            '$img.ext$', '$img.uploaded_at.timestamp$', '$img.uploaded_at.utc$', '$img.size$', '$host.name$',
            '$host.motd$']
    return render_template("embed_conf.html",
                           form=form,
                           username=user.username, vars=vars)


@app.route("/url-conf/", methods=['GET', 'POST'])
def url_conf():
    user: User = User.query.filter_by(uid=session['uid']).first()

    class UrlConfigForm(Form):
        invisibleurl = BooleanField(
            "Invisible URLs", default="checked" if user.invisibleurls else "")

    form = UrlConfigForm(request.form)
    if request.method == 'POST' and form.validate():
        user.invisibleurls = form.invisibleurl.data
        db.session.commit()

        flash("URL preferences successfully set!", 'success')

    return render_template("url_conf.html", form=form,
                           username=user.username)


@app.route("/account/")
@login_required
def account():
    user: User = User.query.filter_by(uid=session['uid']).first()

    return render_template(
        "account.html",
        username=user.username,
        uid=user.uid,
        email=user.email,
        key=user.key,
    )


class ChangePasswordForm(Form):
    old_password = PasswordField('Old Password', [
        validators.DataRequired(),
    ])
    password = PasswordField('New Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password', [validators.DataRequired()])


@app.route("/change-password/", methods=['GET', 'POST'])
@login_required
def change_password():
    user: User = User.query.filter_by(uid=session['uid']).first()

    form = ChangePasswordForm(request.form)
    if request.method == 'POST' and form.validate():
        if sha256_crypt.verify(form.old_password.data, user.password_hash):
            password = sha256_crypt.encrypt((str(form.password.data)))
            user.password_hash = password
            db.session.commit()
            flash("Password changed successfully.", 'success')
            return redirect(url_for("dashboard.dashboard"))
        else:
            flash("Invalid old password", 'danger')
            return render_template("change_password.html",
                                   form=form, username=user.username)

    return render_template("change_password.html",
                           form=form,
                           username=user.username)


@app.route("/delete-account/")
@login_required
def delete_account():
    usr_images = Image.query.filter_by(user=session['uid']).all()

    for img in usr_images:
        file = img.id + img.ext
        os.remove(os.path.join(current_app.config['SHARX_CONFIG']['storage_folder'], file))

    db.session.delete(User.query.filter_by(uid=session['uid']).first())
    db.session.commit()

    flash("Account deleted successfully", 'success')
    return redirect(url_for("users.logout"))


@app.route("/regenerate-key/")
@login_required
def regenerate_key():
    user: User = User.query.filter_by(uid=session['uid']).first()
    user.key = f"{user.username}_{''.join(random.choice(string.ascii_letters) for _ in range(10))}"
    db.session.commit()

    flash("Key regenerated successfully, please re-download your config!", 'success')
    return redirect(url_for("dashboard.dashboard"))


@app.route("/gallery/")
@login_required
def gallery():
    imgs = Image.query.filter_by(user=session['uid']).all()
    user: User = User.query.filter_by(uid=session['uid']).first()

    imgs.reverse()

    # return render_template(
    #     "gallery.html",

    #     username=user.username,
    #     imgs=imgs, )
    flash("The gallery is temporarily suspended, we're sorry for the inconvenience", 'danger')
    return redirect(url_for('dashboard.dashboard'))


@app.route("/gallery/delete-image/<id>/")
@login_required
def delete_image(id):
    user: User = User.query.filter_by(uid=session['uid']).first()

    image: Image = Image.query.filter_by(id=id).first()

    if image is None:
        flash("Image not found!", 'danger')
        return redirect(url_for("dashboard.gallery"))
    if str(image.user) != str(session['uid']):
        flash("Image not owned by user!", 'danger')
        return redirect(url_for("dashboard.gallery"))

    os.remove(os.path.join(current_app.config['SHARX_CONFIG']['storage_folder'], str(
        user.uid), id + image.ext))
    user.storage_used -= image.size_b
    db.session.delete(image)
    db.session.commit()

    flash("Image deleted successfully", 'success')
    return redirect(url_for("dashboard.gallery"))
