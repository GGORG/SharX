import json
import random
import string
import sys
# import sqlite3
import subprocess

from flask import request, url_for, jsonify, \
    Blueprint, current_app
import flask
from database import *

app = Blueprint('api', __name__, url_prefix='/api')


### API I GUESS

@app.route("/update-git", methods=['POST'])
def update_git():
    rand = random.Random(current_app.config['SECRET_KEY'])
    key = ''.join(rand.choice(string.ascii_letters) for _ in range(16))
    if request.headers['X-Gitlab-Token'] != key:
        print(" * Update-git key: " + key)
        return "wrong key", 401

    from ctl import update
    update()
    return "success"

@app.route("/version")
def version():
    remote = subprocess.check_output(["git", "remote", "get-url", 'origin']).strip().decode()
    commit = subprocess.check_output(["git", "describe", "--always"]).strip().decode()
    full_commit = subprocess.check_output(["git", "rev-parse", "HEAD"]).strip().decode()
    tag = subprocess.check_output(["git", "describe", "--tags"]).strip().decode()
    pyver = sys.version
    flaskver = flask.__version__
    gitstatus = subprocess.check_output(["git", "status", "--short"]).strip().decode()
    wsgi = current_app.config['SHARX_CONFIG']['wsgi']
    return jsonify({
        "git": {
            "remote": remote,
            "commit": {
                "short": commit,
                "full": full_commit,
            },
            "tag": tag,
            "status": gitstatus
        },
        "pyver": pyver,
        "flaskver": flaskver,
        "version": current_app.config['SHARX_HOST_VERSION'],
        "wsgi": wsgi
    })
