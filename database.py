from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class User(db.Model):
    __tablename__ = 'users'
    uid = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password_hash = db.Column(db.String(77), nullable=False)
    key = db.Column(db.String(30), unique=True, nullable=False)
    storage_used = db.Column(db.Integer, nullable=False)
    invisibleurls = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return '<User %r>' % self.username


class Image(db.Model):
    __tablename__ = 'images'
    id = db.Column(db.String(8), unique=True, nullable=False, primary_key=True)
    name = db.Column(db.String(40), nullable=False)
    ext = db.Column(db.String(5), nullable=False)
    upload_time = db.Column(db.Integer, nullable=False)
    size_b = db.Column(db.Integer, nullable=False)
    user = db.Column(db.Integer, nullable=False)


class Invite(db.Model):
    __tablename__ = 'invites'
    code = db.Column(db.String(16), unique=True,
                     nullable=False, primary_key=True)
    user = db.Column(db.Integer, nullable=False)


class InvisibleUrl(db.Model):
    __tablename__ = 'invisibleurls'
    url = db.Column(db.Text(), unique=True,
                    nullable=False, primary_key=True)
    imgid = db.Column(db.String(8), unique=True, nullable=False)


class Embed(db.Model):
    __tablename__ = 'embeds'
    user = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    color = db.Column(db.String(7), nullable=False)
    title = db.Column(db.Text, nullable=False)
    desc = db.Column(db.Text, nullable=False)
    author_name = db.Column(db.Text, nullable=False)
    author_url = db.Column(db.Text, nullable=False)
    provider_name = db.Column(db.Text, nullable=False)
    provider_url = db.Column(db.Text, nullable=False)

class ImgHash(db.Model):
    __tablename__ = 'imghashes'
    hash = db.Column(db.Text(), 
                    nullable=False, primary_key=True)
    imgid = db.Column(db.String(8), unique=True, nullable=False)
    user = db.Column(db.Integer, nullable=False)
